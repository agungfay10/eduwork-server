const { subject } = require("@casl/ability");
const Invoice = require("../invoice/model");
const { policyFor } = require("../../utils");

const show = async (req, res, next) => {
  try {
    let { order_id } = req.params;
    console.log(order_id);
    let invoice = await Invoice.findOne({ order: order_id })
      .populate("order")
      .populate("user");

    let policy = policyFor(req.user);
    let subjectInvoice = subject("Invoice", {
      ...invoice,
      user_id: invoice.user._id,
    });
    if (!policy.can("read", subjectInvoice)) {
      return res.json({
        error: 1,
        message: `Anda tidak memiliki akses untuk melihat invoice ini.`,
      });
    }
    return res.json(invoice);
  } catch (err) {
    return res.json({
      error: 1,
      message: err.message,
    });
  }
};

const update = async (req, res) => {
  try {
    const invoicesId = req.params.id;
    const paid = "paid";

    const invoices = await Invoice.findById(invoicesId);

    if (!invoices) {
      return res.status(404).json({ message: "invoice tidak ditemukan" });
    }

    invoices.payment_status = paid;

    await invoices.save();
    return res.json({ message: "Kolom berhasil diubah" });
  } catch (error) {
    console.error("Terjadi kesalahan:", error);
    return res
      .status(500)
      .json({ message: "Terjadi kesalahan saat mengubah kolom" });
  }
};

module.exports = {
  show,
  update
};
